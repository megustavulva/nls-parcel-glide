module.exports = {
  theme: {
    extend: {},
    fontSize: {
      'sm': '.675rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.5rem',
      '2xl': '2rem',
      '3xl': '2.5rem',
      '4xl': '5rem'
    },
    opacity: {
      '0': '0',
      '5': '.05',
      '10': '.1',
      '25': '.25',
      '50': '.5',
      '75': '.75',
      '100': '1',
    },
    fontFamily: {
      'display': 'Heebo, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      'sans': 'Heebo, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      'serif': 'Vollkorn, Georgia, Cambria, "Times New Roman", Times, serif'
    }
  },
  container: false,
  variants: {
    textColor: ['hover'],
  },
  plugins: []
}
