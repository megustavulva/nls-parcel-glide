import  './styles/index.scss'
import Glide from '@glidejs/glide'
import './scripts/appHeight'

const defaultSettings = {
  type: 'slider',
  focusAt: 'center',
  gap: 0,
  touchRatio: 1,
  perView: 5
}
const glideClass = '.glide'
const slideClass = '.glide__slide'
const contentClass = '.glide__slide__content'
const sectionClass = 'section'
const articleClass = 'article'
const contentCloneClassName = 'content'
const glide = new Glide(glideClass, defaultSettings)
const glideEl = document.querySelector(glideClass)
const sectionEl = document.querySelector(sectionClass)
const articleEl = document.querySelector(articleClass)
const allSlidesEl = document.querySelectorAll(slideClass)
const copyrightEl = document.createElement('div')

let inAction = false
let hasInteracted = false
let inDetails = null

copyrightEl.classList.add('container')
copyrightEl.innerHTML = '<p class="mt-10 text-sm">© 2019 by Michael Werner Czechowski – <a>Imprint</a></p>'

copyrightEl.querySelector('a').addEventListener('click', e => {
  glide.go('=6')
})


allSlidesEl.forEach((slide, index) => {
  const content = slide.querySelector(contentClass)

  slide.addEventListener('click', e => {
    if (glide.index === index) {
      toggleDetails(index)
    } else {
      glide.go(`=${index}`)
    }

    hasInteracted = true
  })
})

document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    if (!hasInteracted) {
      glide.go(`=1`)
      window.scrollTo(0, 0)
    }
  }, 200)
})

glide.on('swipe.start', () => {
  if (!inAction) {
    inAction = true
    zoomOut()
  } 
  hasInteracted = true
})

glide.on('swipe.end', () => {
  if (inAction) {
    inAction = false
    zoomIn()
  }
  hasInteracted = true
})

glide.on('run', () => {
  toggleDetails(glide.index)
})

glide.mount()

// --------------------------------

function zoomIn() {
  sectionEl.classList.remove('--swiping')
}

function zoomOut() {
  sectionEl.classList.add('--swiping')
}

function toggleDetails(index) {
  const slide = allSlidesEl[index]
  const content = slide.querySelector(contentClass)
  const prevContentEl = articleEl.querySelector(`.${contentCloneClassName}`)

  window.scrollTo(0, 0)

  if (prevContentEl) {
    prevContentEl.remove()
  }

  if (content) {
    const contentEl = content.cloneNode(true)

    if (inDetails === index) {
      sectionEl.classList.remove('--full-screen')
      inDetails = null
    } else {
      contentEl.className = contentCloneClassName
      sectionEl.insertAdjacentElement('afterEnd', contentEl)
      contentEl.insertAdjacentElement('beforeEnd', copyrightEl)
      sectionEl.classList.add('--full-screen')

      inDetails = index
    }
  } else {
    sectionEl.classList.remove('--full-screen')
    inDetails = index
  }
  hasInteracted = true
}

